# e3-cfhvacms

Wrapper for the module cfhvacms

<!-- This README.md should be updated as part of creation and should add complementary information about the wrapped module in question (usage, etc.). Once the repository is set up, empty/unused directories should also be purged. -->

## Requirements

<!-- Put requirements here, like:
- pytest
- pyepics
-->
- pytest
- pyepics
- opcua
- run-iocsh

## EPICS dependencies

<!-- Run `make dep` and put the results here, like:
```sh
$ make dep
require examplemodule,1.0.0
< configured ...
COMMON_DEP_VERSION = 1.0.0
> generated ...
common 1.0.0
```
-->
- e3-opcua module


## Installation

```sh
$ make build
$ make cellinstall
$ make test
```

For further targets, type `make`.

## Usage

To load the e3-cfhvacms module:
```sh
$ iocsh -r "cfhvacms"
```

## Additional information

<!-- Put design info or links (where the real pages could be in e.g. `docs/design.md`, `docs/usage.md`) to design info here.
-->

Current version of the test server only tests 12 nodes that picked randomly from "hvac_process_xml.xml" file to prove that:

   
	1.) this module(IOC) can automatically test those nodes of Hvac server that we are interested.

   	2.) this module(IOC) can be used to mornitor/control the Hvac server through those nodes.

The "hvac_process_xml.xml" was exported by Karl from a runing Hvac server.

The Hvac nodes used in this version are:
```
    	System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value
	System1.OPC:OPC.D02.A.Ahu201.Azo2.TR.Present_Value
	System1.OPC:OPC.D02.A.Ahu200.TEx.Present_Value
	System1.OPC:OPC.D02.A.Ahu100.TEx.Present_Value
	System1.OPC:OPC.G02.Process.EC001.CWH.FT001.PV.Value
	System1.OPC:OPC.G02.Process.EC001.CWH.CV004.PV.Value
	System1.OPC:OPC.G02.Process.EC001.CWH.CT010.PV.Value
	System1.OPC:OPC.G02.Process.EC001.CWH.CT009.PV.Value
   	System1.OPC:OPC.F03.A.Ahu101.Alm.Present_Value
	System1.OPC:OPC.G01.A.Ahu400.Azo.TR.Present_Value
	System1.OPC:OPC.G02.Process.EC003.DIW.FT009.PV.Value
	System1.OPC:OPC.G02.Process.EC003.IAR.PT021.PV.Value  
```
## Contributing

Contributions through pull/merge requests only.
