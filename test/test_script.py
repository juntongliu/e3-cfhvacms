import os
import signal
import subprocess
from datetime import datetime
from os import environ
from pathlib import Path
from time import sleep

import pytest
from epics import PV, ca
from run_iocsh import IOC

os.environ["IOCNAME"] = "Pwr:SC-IOC-001"


class opcuaTestHarness:
    def __init__(self):
        # timeout value in seconds for pvput/pvget calls
        self.timeout = 5
        self.putTimeout = self.timeout
        self.getTimeout = self.timeout

        # Test server
        self.testServer = Path(__file__).parent / "server" / "cfhvacmsTestServer"
        self.isServerRunning = False
        self.serverURI = "opc.tcp://localhost.localdomain:4840"
        self.serverFakeTime = "2019-05-02 09:22:52"

        # Message catalog
        self.connectMsg = (
            "OPC UA session OPC1: connection status changed"
            + " from Connected to Disconnected"
        )
        self.reconnectMsg = (
            "OPC UA session OPC1: connection status changed"
            + " from ConnectionErrorApiReconnect to NewSessionCreated"
        )
        self.reconnectMsg1 = (
            "OPC UA session OPC1: connection status changed"
            + " from NewSessionCreated to Connected"
        )
        self.noConnectMsg = (
            "OPC UA session OPC1: connection status changed"
            + " from Disconnected to ConnectionErrorApiReconnect"
        )

        self.badNodeIdMsg = "item ns=2;s=Sim.BadVarName : BadNodeIdUnknown"

        # Server variables
        self.serverVars = [
            "open62541",
            "open62541 OPC UA Server",
            "1.2.0-29-g875d33a9",
        ]

    def start_server(self, withPIPE=False):
        if withPIPE:
            self.serverProc = subprocess.Popen(
                [self.testServer],
                shell=False,
                stdout=subprocess.PIPE,
            )
        else:
            self.serverProc = subprocess.Popen(
                [self.testServer],
                shell=False,
            )

        print("\nOpening server with pid = %s" % self.serverProc.pid)
        retryCount = 0
        while (not self.isServerRunning) and retryCount < 5:
            # Poll server to see if it is running
            self.is_server_running()
            retryCount = retryCount + 1
            sleep(1)

        assert retryCount < 5, "Unable to start server"

    def start_server_with_faketime(self):
        self.serverProc = subprocess.Popen(
            f"faketime -f '{self.serverFakeTime}' {self.testServer}",
            shell=True,
            preexec_fn=os.setsid,
        )

        print("\nOpening server with pid = %s" % self.serverProc.pid)
        retryCount = 0
        while (not self.isServerRunning) and retryCount < 5:
            # Poll server to see if it is running
            self.is_server_running()
            retryCount = retryCount + 1
            sleep(1)

        assert retryCount < 5, "Unable to start server"

    def stop_server_group(self):
        # Get the process group ID for the spawned shell,
        # and send terminate signal
        print("\nClosing server group with pgid = %s" % self.serverProc.pid)
        os.killpg(os.getpgid(self.serverProc.pid), signal.SIGTERM)
        # Update if server is running
        self.is_server_running()

    def stop_server(self):
        print("\nClosing server with pid = %s" % self.serverProc.pid)
        # Send terminate signal
        self.serverProc.terminate()
        # Wait for processes to terminate.
        self.serverProc.wait(timeout=20)
        # Update if server is running
        self.is_server_running()

    def is_server_running(self):
        from opcua import Client

        c = Client(self.serverURI)
        try:
            # Connect to server
            c.connect()
            # NS0|2259 is the server state variable
            # 0 -- Running
            var = c.get_node("ns=0;i=2259")
            val = var.get_data_value()
            self.isServerRunning = val.StatusCode.is_good()
            # Disconnect from server
            c.disconnect()

        except Exception:
            self.isServerRunning = False


# Standard test fixture
@pytest.fixture(scope="function")
def test_inst():
    """
    Instantiate test harness, start the server,
    yield the harness handle to the test,
    close the server on test end / failure
    """
    # Initialize channel access
    ca.initialize_libca()

    # Create handle to Test Harness
    test_inst = opcuaTestHarness()
    # Poll to see if the server is running
    test_inst.is_server_running()
    assert not (
        test_inst.isServerRunning
    ), "An instance of the OPC-UA test server is already running"
    # Start server
    test_inst.start_server()
    # Drop to test
    yield test_inst

    # Shutdown server by sending terminate signal
    test_inst.stop_server()
    # Check server is stopped
    assert not test_inst.isServerRunning

    # Shut down channel access
    ca.flush_io()
    ca.clear_cache()


# test fixture for use with timezone server
@pytest.fixture(scope="function")
def test_inst_TZ():
    """
    Instantiate test harness, start the server,
    yield the harness handle to the test,
    close the server on test end / failure
    """
    # Initialize channel access
    ca.initialize_libca()

    # Create handle to Test Harness
    test_inst_TZ = opcuaTestHarness()
    # Poll to see if the server is running
    test_inst_TZ.is_server_running()
    assert not (
        test_inst_TZ.isServerRunning
    ), "An instance of the OPC-UA test server is already running"
    # Start server
    test_inst_TZ.start_server_with_faketime()
    # Drop to test
    yield test_inst_TZ

    # Shutdown server by sending terminate signal
    test_inst_TZ.stop_server_group()
    # Check server is stopped
    assert not test_inst_TZ.isServerRunning

    # Shut down channel access
    ca.flush_io()
    ca.clear_cache()


@pytest.fixture(scope="function")
def test_ioc():
    """
    Instantiate a test IOC.

    Start it, Yield it to the test function, shut it down.
    """
    env_vars = ["EPICS_BASE", "E3_REQUIRE_VERSION", "TEMP_CELL_PATH"]
    base, require, cell_path = map(environ.get, env_vars)
    if not cell_path:
        cell_path = Path(__file__).parent.parent / "cellMods"
    else:
        cell_path = Path(cell_path)
    assert cell_path.is_dir()

    assert base
    assert require

    iocsh_path = Path(base) / "require" / require / "bin" / "iocsh"
    assert iocsh_path.is_file()

    test_cmd = Path(__file__).parent / "st_test.cmd"
    assert test_cmd.is_file()

    args = ["-l", cell_path, test_cmd]
    ioc = IOC(*args, ioc_executable=iocsh_path, timeout=20.0)
    ioc.start()
    assert ioc.is_running()

    ioc.sleep_time = 3.0
    sleep(ioc.sleep_time)

    yield ioc

    ioc.exit()
    assert not ioc.is_running()


class TestUnitTests:
    def test_System1_Nodes_PVs(self, test_inst, test_ioc):
        """
        Start the server, start the IOC.
        Read System1 D02 present values
        """

        # Read system1 D02 Ahu100 TEx
        pvName = "system1_D02_A_Ahu100_TEx_Present_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 21.52099609375
        assert pv.severity == 0

        # Read system1 D02 Ahu201 Azo2 TR 
        pvName = "system1_D02_A_Ahu201_Azo2_TR_Present_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 19.0787658691406
        assert pv.severity == 0

        # Read system1 D02 Ahu200 TEx 
        pvName = "system1_D02_A_Ahu200_TEx_Present_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 18.8894958496094
        assert pv.severity == 0

        # Read system1 D04 Ahu109 TEx 
        pvName = "system1_D02_A_Ahu100_TEx_Present_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 21.52099609375
        assert pv.severity == 0

        pvName = "system1_G02_Process_EC001_CWH_FT001_PV_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 42.4714279174805
        assert pv.severity == 0

        pvName = "system1_G02_Process_EC001_CWH_CV004_PV_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 8.25376129150391
        assert pv.severity == 0

        pvName = "system1_G02_Process_EC001_CWH_CT010_PV_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 0.10416666418314
        assert pv.severity == 0

        pvName = "system1_G02_Process_EC001_CWH_CT009_PV_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue ==  0.119285300374031
        assert pv.severity == 0

        pvName = "system1_F03_A_Ahu101_Alm_Present_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue ==  0
        assert pv.severity == 0

        pvName = "system1_G01_A_Ahu400_Azo_TR_Present_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue ==  18.6312255859375
        assert pv.severity == 0

        pvName = "system1_G02_Process_EC003_DIW_FT009_PV_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue ==  0
        assert pv.severity == 0

        pvName = "system1_G02_Process_EC003_IAR_PT021_PV_Value"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue ==  6.86863422393799
        assert pv.severity == 0


#    def test_System1_PV_Value(self, test_inst, test_ioc):
#        """
#        Start the server, start the IOC.
#        Read System1 D02 PV values
#        """
#
#        # Read system1 D02 Ahu100 TEx
#        pvName = "system1_G02_Process_EC001_CWH_FT001_PV_Value"
#        pv = PV(pvName)
#        pvalue = pv.get(timeout=test_inst.getTimeout)
#        assert pvalue == 42.4714279174805
#        assert pv.severity == 0



