# Test - CFHVACMS
This directory contains the sources for the automatic testing of the e3-cfhvacms module.

## Prerequisites
In order to run the test suite, you must install the following:

 * python3
 * libfaketime
 * cmake

On CentOS 7, run the following:

```
$ sudo yum install -y python3 libfaketime
$ sudo yum install cmake
```

And the following python modules:

 * pytest
 * pyepics
 * opcua
 * run-iocsh

You can use the following pip3 commands:

```
$ pip3 install pytest opcua pyepics
$ pip3 install run-iocsh -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
```

You must configure the EPICS environment before running the test suite.
For the E3 environment, this requires you to ``source setE3Env.bash``.

Finally, compile the test server for use by the test suite:
```
$ cd test/server
make
```

## Tests
The tests for this test scrip are defined in the varification and validation plan for the CFHVACMS [1]


## Running the test suite
The primary way to run the is to run the following from the e3-cfhvacms root:
```
$ make test
```

You can also run the test suite from this folder using the following command, assuming you have installed in cell mode (make cellinstall):
```
$ pytest -v test_script.py
```

## Expected result
A successful run should look something like this:
```
================================== test session starts ===================================
platform linux -- Python 3.6.8, pytest-7.0.1, pluggy-1.0.0 -- /usr/bin/python3
cachedir: .pytest_cache
rootdir: /home/vagrant/tmp/ua_testServer
collected 1 item                                                                                           

test/test_script.py::TestUnitTests::test_System1_Nodes_PVs PASSED                [100%]

============================================ 1 passed in 6.67s ===========================
```

## Compile XML file into C file
The test server used here is an open62541 UA server. It is built with C and header files generated from XML file(UA information model). Open62541 comes with a nodeset-compiler and other files for this purpose. So, to be able to compile an UA information model(XML file) into C files, one needs to download, build and install open62541. For detail information, check this link: http://www.open62541.org/doc/1.3/nodeset_compiler.html

Following are steps to download, build and install open62541:
```
$ git clone https://github.com/open62541/open62541.git
$ cd open62541
$ git checkout v1.3.2
$ mkdir build && cd build
$ cmake -DCMAKE_INSTALL_PREFIX=/path/to/install-dir -DCMAKE_BUILD_TYPE=RelWithDebInfo -DUA_NAMESPACE_ZERO=Full -DUA_ENABLE_AMALGAMATION=On -DCMAKE_C_COMPILER=gcc ..
$ make -j
$ make install
```
This will build and install open62541 into "/path/to/install-dir" directory.
The nodeset compiler will be installed at "/path/to/install-dir/share/open62541/tools/nodeset_compiler/".

The nodesest compile will use "Opc.Ua.NodeSet2.xml" file when compile a XML file into C files. The "Opc.Ua.NodeSet2.xml" file can be found at: "/path/to/install-dir/share/open62541/tools/ua-nodeset/Schema/" directory.

When an UA information model(the XML file) is ready, the follwoing command can be used to compile the XML file into C files:

```
$ python3 nodeset_compiler.py --types-array=UA_TYPES --existing /path/to/install-dir/share/open62541/tools/ua-nodeset/Schema//Opc.Ua.NodeSet2.xml --xml my_model.xml my_cfile_prefix
```
This command will generate one C file and one C header file named: "my_cfile_prefix.c" and "my_cfile_prefix.h". This C files are the main part of the UA test server. To compile the newly generated C files into the test server, the Makefile and cfhvacmsTestServer.c file might need to be modified depending on the file name one use.

If one use ones own file name (my_cfile_prefix), the Makefile and the cfhvacmsTestServer.c file in the "test/server/" directory need to be modified to make the compiling work. This is because that the file name is included in the generated C and header files by the tool. So, it is better that one uses the same file name in the "test/server/" directory when one compiles a XML file into C file. And then just copy the new files into the server directory to overwrite the old files. No modification to the Makefile and cfhvacmsTestServer.c files is required in this case. 

For now, the file name is Hvac_Simulation_NodeSet". So, the above command will be:

```
$ python3 nodeset_compiler.py --types-array=UA_TYPES --existing  /path/to/install-dir/share/open62541/tools/ua-nodeset/Schema//Opc.Ua.NodeSet2.xml --xml my_model.xml  Hvac_Simulation_NodeSet

$ cp Hvac_Simulation_NodeSet.*  /path/to/test/server/directory/
$ cd /path/to/test/server/directory
$ make                    # build the server with new files
$ ./cfhvacmsTestServer    # test run the newly compiled test server

```


Additional information about how to use docker to do the compiling can be found at: https://opcua.rocks/step-7-compile-the-modeldesign-file-with-ua-modelcompiler/
(A step by step instructions will be added here later when I finish reading some documentions).
 

## References
[1] [\[ESS-0145304\]](https://chess.esss.lu.se/enovia/link/ESS-0145304/21308.51166.51456.27357/valid) CF HV Monitoring System - Verification and Validation Plan

