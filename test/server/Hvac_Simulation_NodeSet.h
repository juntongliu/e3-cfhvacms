/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#ifndef EPICSOPEN62541SERVERNS3FORIOC_H_
#define EPICSOPEN62541SERVERNS3FORIOC_H_


#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include <open62541/server.h>
#endif



_UA_BEGIN_DECLS

//extern UA_StatusCode epicsOpen62541ServerNs3ForIoc(UA_Server *server);
extern UA_StatusCode Hvac_Simulation_NodeSet(UA_Server *server);

_UA_END_DECLS

#endif /* EPICSOPEN62541SERVERNS3FORIOC_H_ */
