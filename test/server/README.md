This directory contains a cfhvacms test UA server and a client tool.
The cfhvacmsTestServer will be launched to do tests when a "make test" command is issued at the top directory of e3-cfhvacms module after build and cellinstall.

One can also run the cfhvacmsTestServer directly as following:

	$ cd /path/to/e3-cfhvacms/test/server
	$ make
	$ ./cfhvacmsTestServer

As how to use the UA client tool, please read the comments at the begining of the client.c file. 

